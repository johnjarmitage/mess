function out=readX_name(dirname,filenumber,varnumber);
filename=[num2str(filenumber) '.txt'];
fid=fopen([dirname filename],'rb');
N=fread(fid,1,'int32');

filename=['X' num2str(filenumber) '.txt'];
fid=fopen([dirname filename],'rb');

begin=[0];
begin(end+1)=8*N+begin(end); % X
begin(end+1)=8*N+begin(end); % dm
begin(end+1)=8*N+begin(end); % XComp
begin(end+1)=8*N+begin(end); % RComp

Nel=begin(varnumber+1)-begin(varnumber);
fseek(fid,begin(varnumber),-1);
Nbytes=Nel/8;
out=fread(fid,Nbytes,'real*8');

fclose(fid);