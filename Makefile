# make file for Kenni Dinese Petersen's code

# if on glmac remember to run >> source /opt/intel/bin/compilervars.sh intel64
# if on malbec remember to run >> module load intel/compiler
# if on ese-armitage will use gcc

## glmac options
#CC = icc
#CFLAGS = -O2 -xHOST -openmp

## malbec options
CC = icc
CFLAGS = -O2 -xAVX -qopenmp
LIB = 

## ese-armitage options
#CC = gcc
#CFLAGS = -fopenmp
#LIB = -lm

melting:
	$(CC) $(CFLAGS) main.c $(LIB) -o melting

clean:
	rm melting
