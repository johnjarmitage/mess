% clear all;
% close all;

dir = './2D_Tp1400/outfiles/';

% List of outputted properties and how they are opened from a file with number "count". All units are S.I.
%     N=readdata_name(dir,count,1);               % Number of particles
%     S=readdata_name(dir,count,2);               % Number of grid nodes in x-direction
%     T=readdata_name(dir,count,3);               % as above, z-direction
%     multigridlevels=readdata_name(dir,count,4); % number of multigrid levels (has no practical use in terms of plotting)
%     bound=readdata_name(dir,count,5);           % types of boundaries. not important
%     time=readdata_name(dir,count,6);
%     dtnom=readdata_name(dir,count,7);           % length of time step used in model run not important for plotting
%     dx=readdata_name(dir,count,8);              % distance between grid nodes (width of cells) in x-direction: Vector, size S-1
%     dz=readdata_name(dir,count,9);              % as above, z-direction. size T-1
%     mx=readdata_name(dir,count,10);             % marker x positions, vector size N (all properties below starting with an are vector size N)
%     mz=readdata_name(dir,count,11);             % marker z positions, vector size N
%     mcs=readdata_name(dir,count,12);            % marker belongs to cell with horizontal index specified in this variable (value between 0 and S-1). Can actually be calculated from mx, and dx, so redundant
%     mct=readdata_name(dir,count,13);            % as above, z-direction
%     mtype=readdata_name(dir,count,14);          % what type of material are each marker
%     mTemp=readdata_name(dir,count,15);          % temperature
%     mstressxx=readdata_name(dir,count,16);      % devitoric stress component simga'xx=-sigma'zz
%     mstressxz=readdata_name(dir,count,17);      % sigma'xz=sigmaxz
%     mstrainxx=readdata_name(dir,count,18);      % similar deviatoric strain components (eps'ij=epsij due to incompressibility)
%     mstrainxz=readdata_name(dir,count,19);
%     mfinitestrain=readdata_name(dir,count,20);  % accumulated plastic strain (other types of strain are not tracked and saved) 
%     vx=readdata_name(dir,count,21);             % horisontal velocity grid. Displaced by 0.5 dz relative to primary grid. Matrix, size S*T (includes some dummy points that are outside of the computational domain) 
%     vz=readdata_name(dir,count,22);             % horisontal velocity grid. Displaced by 0.5 dx relative to primary grid. Matrix, size S*T (includes some dummy points that are outside of the computational domain) 
%     P=readdata_name(dir,count,23);              % presseure=-0.5(sigma_xx+sigma_zz). displaced 0.5(dx,dz) Matrix, size S*T (includes even more dummy points)
%     Nsurfaces=readdata_name(dir,count,24);      % Number of semi eulerian surfaces used for tracking interfaces that are not strongly deformed 
%     surfaces=readdata_name(dir,count,25);       % surfaces. Matrix, size S*Nsurfaces
%     meltFraction=readX(dir,count);         % melt fraction

for count = 13
    
    time    = readdata_name(dir,count,6);
    
    mtype             = readdata_name(dir,count,14);
    mtemp             = readdata_name(dir,count,15);
    mtemp(mtype == 0) = NaN;
    S                 = readdata_name(dir,count,2);
    T                 = readdata_name(dir,count,3);
    dx                = readdata_name(dir,count,8);
    dz                = readdata_name(dir,count,9);
    mx                = readdata_name(dir,count,10);
    mz                = readdata_name(dir,count,11);
    xmax              = sum(dx);
    zmax              = sum(dz);
    xi                = linspace(0,xmax,S*2);
    zi                = linspace(0,zmax,T*2);              %size of grid need not be 2*S*2*T, but it gives a scaling of the resolution since N is usually proportional to S*T (typically 12-16 markers are present in each cell during initiation)
    F                 = TriScatteredInterp(mx,mz,mtemp); %calculates interpolation function (based on triangulization)
    [Xi,Zi]           = meshgrid(xi,zi);                   %xi and zi are vectors with the nodes of the grid we have specified. Xi,Zi are matrices with the same, but now redunant values
    grid              = F(Xi,Zi);
    X                 = 1-1./readX_name(dir,count,1);
    dm                = readX_name(dir,count,2);
    XF                = zeros(size(X));
    XF(dm>0)          = X(dm>0);
    %XF                = dm;
    XF(mtype == 0)    = NaN;
    meltF             = TriScatteredInterp(mx,mz,XF);
    gridF             = meltF(Xi,Zi);
        
    mstresstot       = (readdata_name(dir,count,16).^2 + readdata_name(dir,count,17).^2).^0.5;
    mstraintot       = (readdata_name(dir,count,18).^2 + readdata_name(dir,count,19).^2).^0.5;
    meta             = 0.5*mstresstot./mstraintot;
    meta(mtype == 0) = NaN;
    eta              = TriScatteredInterp(mx,mz,log10(meta));
    grideta          = eta(Xi,Zi);
    
    Ztop = 1e-3*min(Zi(~isnan(grid(:))));
        
    fig1=figure(11);
    subplot(3,1,1)
    contourf(1e-3*Xi,1e-3*Zi-Ztop,grideta,50);
    shading flat;
    axis ij;
    %axis([900 1500 0 300]);
    title_text = sprintf('log_{10}(Viscosity), Time = %.1f',time/(60*60*24*365*1e6));
    title(title_text);
    hc=colorbar('vert');
    set(get(hc,'YLabel'),'String','log_{10} Pa s');
        
    subplot(3,1,2)
    contourf(1e-3*Xi,1e-3*Zi-Ztop,grid,50);
    shading flat;
    axis ij;
    %axis([900 1500 0 300]);
    title('Temperature')
    hc=colorbar('vert');
    set(get(hc,'YLabel'),'String','\circ C')
        
    subplot(3,1,3)
    contourf(1e-3*Xi,1e-3*Zi-Ztop,gridF,50);
    shading flat;
    axis ij;
    %axis([900 1500 0 300]);
    title('Melt depletion')
    hc=colorbar('vert');
    set(get(hc,'YLabel'),'String','F')
    
    drawnow
    pause(0.20);
    
end