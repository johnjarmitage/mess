#include "math.h"
#include "mex.h"
#include "ketch.c"

/*
   int  ForwardModel
                    int       numTTDefs,
                    double    stdLengthReduction,
                    double    kinPar,
                    double    pctPerTimeStep,
                    int       annealModel,
                    int       doProject,
                    int       usedCf,
                    int       kinParType,
                    int       l0model,
                    double    l0user,
                    double    cdf[],
                    int        numPDFPts,
                    double    pdfAxis[],
                    double    pdf[],
                    double    *oldestModelAge,
                    double    *ftModelAge,
                    int       *numPopulations,
                    ttPathPtr    tTDef
                   

 */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double stdLengthReduction,kinPar,pctPerTimeStep,l0user,*oldestModelAge,*ftModelAge;
    double *cdf,*pdfAxis,*pdf;
    double numTTDefs,annealModel,doProject,usedCf,kinParType,l0model,numPDFPts,*numPopulations;
    ttPathPtr tTDef;
    
    int i,numPopulations_int;
    double *temp, *time;
    
     
    time = mxGetPr(prhs[0]);  numTTDefs=mxGetNumberOfElements(prhs[0]);
    temp = mxGetPr(prhs[1]);
    stdLengthReduction = *mxGetPr(prhs[2]);
    kinPar = *mxGetPr(prhs[3]);
    pctPerTimeStep = *mxGetPr(prhs[4]);
    annealModel = *mxGetPr(prhs[5]);
    doProject = *mxGetPr(prhs[6]);
    usedCf = *mxGetPr(prhs[7]);
    kinParType = *mxGetPr(prhs[8]);
    l0model = *mxGetPr(prhs[9]);
    l0user = *mxGetPr(prhs[10]);
    pdfAxis = mxGetPr(prhs[11]); numPDFPts=mxGetNumberOfElements(prhs[11]);
    
    plhs[0] = mxCreateDoubleMatrix(1,(int)numPDFPts, mxREAL); cdf=mxGetPr(plhs[0]);
    plhs[1] = mxCreateDoubleMatrix(1,(int)numPDFPts, mxREAL); pdf=mxGetPr(plhs[1]);
    plhs[2] = mxCreateDoubleMatrix(1,1, mxREAL); oldestModelAge=mxGetPr(plhs[2]);
    plhs[3] = mxCreateDoubleMatrix(1,1, mxREAL); ftModelAge=mxGetPr(plhs[3]);
    plhs[4] = mxCreateDoubleMatrix(1,1, mxREAL); numPopulations=mxGetPr(plhs[4]);
    
    tTDef=(ttPathRec*)mxCalloc((int)numTTDefs,sizeof(ttPathRec));
    for (i=0;i<(int)numTTDefs;i++) {
        tTDef[i].time=time[i];
        tTDef[i].temp=temp[i];
    }
    
    ForwardModel((int)numTTDefs,stdLengthReduction,kinPar,pctPerTimeStep,(int)annealModel,(int)doProject,(int)usedCf,(int)kinParType,(int)l0model,l0user,cdf,(int)numPDFPts,pdfAxis,pdf,oldestModelAge,ftModelAge,&numPopulations_int,tTDef);
    *numPopulations=(double)numPopulations_int;
    

    mxFree(tTDef);

}
    