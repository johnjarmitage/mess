void tridiag(double *x,double *a,double *b,double *c,double *d,double *bdot,double *ddot,int length) {
    int i;
    double  m;
    
    bdot[0]=b[0];
    ddot[0]=d[0];
    
    for (i=1; i<length; i++) {
        m=a[i]/bdot[i-1];
        bdot[i]=b[i]-m*c[i-1];
        ddot[i]=d[i]-m*ddot[i-1]; }
    x[length-1]=ddot[length-1]/bdot[length-1];
    for (i=length-2; i>=0; i--) {
        x[i]=(ddot[i]-c[i]*x[i+1])/bdot[i];
    }
    
    return;
}

int max(int a, int b) {
    if (a<b)	{
        return(b);	}
    else {
        return(a); }
}

void gettempmarkerparams(double *mC,double *mk,double *mHr) {
    for (e=0;e<N;e++) {
        switch(mtype[e]) {
            case 0:
                mk[e]=3;
                mC[e]=800*2800;
                mHr[e]=0;
                mTemp[e]=Ttop;
                break;
                
            case 1:
                mk[e]=2.5;
                mC[e]=800;
                mHr[e]=1.25e-6;
                break;
                
            case 2:
                mk[e]=2.5;
                mC[e]=800;
                mHr[e]=0.75e-6;
                break;

            case 3:
                mk[e]=4.08*pow((298/(mTemp[e]+273)),0.406);
                mC[e]=(238.64-20.013e2*pow(mTemp[e]+273,-0.5)-11.642e7*pow(mTemp[e]+273,-3))*7.1077;
                mHr[e]=0;
                break;
                
            case 4:
                mk[e]=4.08*pow((298/(mTemp[e]+273)),0.406);
                mC[e]=(238.64-20.013e2*pow(mTemp[e]+273,-0.5)-11.642e7*pow(mTemp[e]+273,-3))*7.1077;
                mHr[e]=0;
                break;
                
            case 5:
                mk[e]=2;
                mC[e]=800;
                mHr[e]=0.5e-6;
                break;
                      
            case 6:
                mk[e]=2.5;
                mC[e]=800;
                mHr[e]=0.75e-6;
                break;
                
                default:
                mk[e]=2;
                mC[e]=800;
                mHr[e]=0;
        }
    }
}

void updatetemp() {
    double *a,*b,*c,*d,*x,*bdot,*ddot;
    double kplus,kcenter,kminus,alpha;
    double *gridC,*gridk,*gridoldTemp,*gridTemp,*gridTempdiff,*gridTempdiffrem,*gridTempdiffsub,*gridHr,*gridH,*gridHa,*weightC,*weightk,*weightoldTemp,*weightTemp,*weightHr;
    double standardC,standardk,standardTemp,standardHr,dt0,dimless;
    double mTempgm,mTempdiffsub;// taras chapt 11 for nomenclature
    double *mC,*mk,*mHr;
    dimless=1; // the name d is already taken
    mC=calloc(N,sizeof(double));
    mk=calloc(N,sizeof(double));
    mHr=calloc(N,sizeof(double));
    a = calloc(max(Sbase,Tbase), sizeof(double));
    b = calloc(max(Sbase,Tbase), sizeof(double));
    c = calloc(max(Sbase,Tbase), sizeof(double));
    d = calloc(max(Sbase,Tbase), sizeof(double));
    x = calloc(max(Sbase,Tbase), sizeof(double));
    bdot = calloc(max(Sbase,Tbase), sizeof(double));
    ddot = calloc(max(Sbase,Tbase), sizeof(double));
    
    gettempmarkerparams(mC,mk,mHr);
    
    gridC=cx0; gridk=cx1; gridoldTemp=cx2; gridTemp=cx3; gridTempdiff=cx4; gridHr=cx5; gridH=cx6;
    weightC=cz0; weightk=cz1; weightoldTemp=cz2; weightTemp=cz3; weightHr=cz4;
    gridHa=resz;
    resetbasegrid(gridC); resetbasegrid(gridk); resetbasegrid(gridoldTemp); resetbasegrid(gridTemp); resetbasegrid(gridTempdiff); resetbasegrid(gridHr); resetbasegrid(gridHa);
    resetbasegrid(weightC); resetbasegrid(weightk); resetbasegrid(weightoldTemp); resetbasegrid(weightTemp); resetbasegrid(weightHr);
    
            
    standardC=0; standardk=0; standardTemp=0; standardHr=0;
    for (e=0;e<N;e++) {
        standardC+=mC[e]; standardk+=mk[e]; standardTemp+=mTemp[e]; standardHr+=mHr[e];
        markertorhogrid(e,mC[e],gridC,weightC);
        markertorhogrid(e,mk[e],gridk,weightk);
        markertorhogrid(e,mHr[e],gridHr,weightHr);
        markertorhogrid(e,mTemp[e],gridoldTemp,weightoldTemp);
    }
    standardC/=N; standardk/=N; standardTemp/=N; standardHr/=N;
    for (s=0;s<Sbase;s++) {
        for (t=0;t<Tbase;t++) {
            st=IN0(s,t);
            if (weightC[st]>0) gridC[st]/=weightC[st]; else { gridC[st]=standardC; }
            if (weightk[st]>0) gridk[st]/=weightk[st]; else { gridk[st]=standardk; }
            if (weightHr[st]>0) gridHr[st]/=weightHr[st]; else { gridHr[st]=standardHr; }
            if (weightoldTemp[st]>0) gridoldTemp[st]/=weightoldTemp[st]; else { gridoldTemp[st]=standardTemp; }
            
        }}
    
    for (s=1;s<Sbase-1;s++) {
        for (t=1;t<Tbase-1;t++) {
            st=IN0(s,t); //LAPPELØSNING for term. ekspansion. Bedst at interpolere fra markers
            gridHa[st]=0.5*gridoldTemp[st]*(2.77e-5+0.97e-8*(gridoldTemp[st]+273)-0.32*pow(gridoldTemp[st]+273,-2))*gridrho[st]*((vx[st]+vx[IN0(s,t-1)])*gx+(vz[st]+vz[IN0(s-1,t)])*gz);
    }}
    for (s=0;s<Sbase;s++) {
        for (t=0;t<Tbase;t++) {
            st=IN0(s,t);
            gridH[st]=gridHr[st]+gridHs[st]+gridHa[st]; //add more eventually
        }}
    for (s=0; s<Sbase; s++) {
        for (t=1; t<Tbase-1; t++) {
            st=IN0(s,t);
            kplus=gridk[IN0(s,t+1)]; kcenter=gridk[st]; kminus=gridk[IN0(s,t-1)];
            alpha=1/(gridrho[st]*gridC[st]*(dz[t-1]+dz[t]));
            a[t]=alpha*(kminus+kcenter)/dz[t-1];
            b[t]=-(1/dt+alpha*(kminus+kcenter)/dz[t-1]+alpha*(kplus+kcenter)/dz[t]);
            c[t]=alpha*(kplus+kcenter)/dz[t];
            d[t]=-1/dt*gridoldTemp[st]-0.5*gridH[st]/(gridrho[st]*gridC[st]); //Kildeleddet bidrager halvt for hver dimension
        }
        b[0]=1;
        c[0]=0;
        d[0]=Ttop;
        a[Tbase-1]=0;
        b[Tbase-1]=1;
        d[Tbase-1]=Tbottom;
        
        tridiag(x,a,b,c,d,bdot,ddot,Tbase);
        for (t=0; t<Tbase; t++) {
            gridTemp[IN0(s,t)]=x[t];
        }
        
    }
    for (t=1; t<Tbase-1; t++) {
        for (s=1; s<Sbase-1; s++) {
            st=IN0(s,t);
            kplus=gridk[IN0(s+1,t)]; kcenter=gridk[st]; kminus=gridk[IN0(s-1,t)];
            alpha=1/(gridrho[st]*gridC[st]*(dx[s-1]+dx[s]));
            a[s]=alpha*(kminus+kcenter)/dx[s-1];
            b[s]=-(1/dt+alpha*(kminus+kcenter)/dx[s-1]+alpha*(kplus+kcenter)/dx[s]);
            c[s]=alpha*(kplus+kcenter)/dx[s];
            d[s]=-1/dt*gridTemp[st]-0.5*gridH[st]/(gridrho[st]*gridC[st]);
        }
        b[0]=-1;
        c[0]=1;
        d[0]=0;
        a[Sbase-1]=-1;
        b[Sbase-1]=1;
        d[Sbase-1]=0;
        
        tridiag(x,a,b,c,d,bdot,ddot,Sbase);
        for (s=0; s<Sbase; s++) {
            gridTemp[IN0(s,t)]=x[s];
        }
    }
    
    gridTempdiffrem=gridHr; //gridHr is not used anymore
    gridTempdiffsub=gridHa; //gridHa is not used anymore
    resetbasegrid(weightTemp); resetbasegrid(gridTempdiffrem); resetbasegrid(gridTempdiffsub);
    
    for (e=0;e<N;e++) {
        dt0=mC[e]*mrho[e]/(2*mk[e]*(1/(dx[mcs[e]]*dx[mcs[e]])+1/(dz[mct[e]]*dz[mct[e]])));
        mTempgm=rhogridtomarker(e,gridoldTemp); //mTempgm is the temperature interpolated from the markers to the grid and back to the markers. It is thus a measure of the numerical diffusion
        mTempdiffsub=(mTempgm-mTemp[e])*(1-exp(-dimless*dt/dt0)); 
        mTemp[e]+=mTempdiffsub; //a portion (depending on 'dimless') of this numerical diffusion is added to the solution
        markertorhogrid(e,mTempdiffsub,gridTempdiffsub,weightTemp); //The numerical diffusion is interpolated back to the grid
    }
    
    for (s=0;s<Sbase;s++){ for (t=0;t<Tbase;t++)	{
        st=IN0(s,t);
        if (weightTemp[st]>0) gridTempdiffsub[st]/=weightTemp[st];  //it is not a big problem if a node is not assigned a weight. the correction will then just be zero
        gridTempdiffrem[st]=gridTemp[st]-gridoldTemp[st]-gridTempdiffsub[st]; //Numerical is subtracted from the temperature correction
    } }
    for (e=0;e<N;e++) {
            mTemp[e]+=rhogridtomarker(e,gridTempdiffrem); //final correction
    }
    
    free(mC);
    free(mk);
    free(mHr);
    free(a);
    free(b);
    free(c);
    free(d);
    free(x);
    free(bdot);
    free(ddot);
}
