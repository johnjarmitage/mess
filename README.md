# README #

MESS: Multigrid Elastic and Stuff Solver
(or Multigrid Elasto-plastic-viscous Strain Solver)

### What is this repository for? ###

This repository is to make available code developed by Kenni Dinesen Petersen (Aarhus) for mantle convection. The code was written by Kenni Dinesen Petersen, with melting routines added by John Armitage.

This is version zero.

### How do I get set up? ###

On linux:

The code is written in C using OpenMP. To compile you need either gcc or icc. See the Makefile for some examples for here at IPGP.
The rheology set up for the various crustal layers is within main.c (from line 467)
The initial condition is created by running the Matlab script makenewmodel.m, where the thermal state of the lithosphere is defined in steadystatelithos1.m. This will make file called 0.txt, which is the initial condition. You then need to make a run directory within which you place 0.txt and make a new directory within called "outfiles". You then run the executable from the run directory, specifying the number of CPUs you wish to use.

The code is quite messy and has not been cleaned up. This is the first time it has been shared, so please be patient and ask if there are problems.

### Contribution guidelines ###

Please if you wish to contribute let Kenni Dinesen Petersen know.

If you use this code in your research please let Kenni Dinesen Petersen know and cite the following article:
Petersen, K. D., Nielsen, S. B., Clausen, O. R., Stephenson, R., & Gerya, T. (2010). Small-scale mantle convection produces stratigraphic sequences in sedimentary basins. Science, 329(5993), 827-830.

### Who do I talk to? ###

The owner of this code is Kenni Dinesen Petersen (kenni@geo.au.dk).

You can also talk to John Armitage (armitage@ipgp.fr)