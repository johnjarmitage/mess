#include "mex.h"
#include <stdio.h>
#include <math.h>
#include <float.h>
int e,s,t,st,N,Sbase,Tbase,Ninlimits,*ind;
double dx,dz,*x,*z;
double *mscalar,*mx,*mz;
double *grid,*weight;
double xmin,xmax,zmin,zmax,mxmin,mxmax,mzmin,mzmax;

int IN0(int s,int t) {
    return t*Sbase+s;
}

void markertorhogrid(int e,double value,double *grid,double *weight) {
    //gives a weighted contribution from a value on a marker to the four sorrounding rho grid points. Division with weights must be performed later
    int s,t;
    int INNV,INNE,INSV,INSE;
    double dlx,dlz,dw;
    s=get_s(mx[e]); t=get_t(mz[e]);
    INNV=IN0(s,t); INNE=IN0(s+1,t); INSV=IN0(s,t+1); INSE=IN0(s+1,t+1);
    dlx=(mx[e]-x[s])/dx;
    dlz=(mz[e]-z[t])/dz;
    // NV
    dw=(1-dlx)*(1-dlz); grid[INNV]+=value*dw; weight[INNV]+=dw;
    // NE
    dw=dlx*(1-dlz); grid[INNE]+=value*dw; weight[INNE]+=dw;
    // SV
    dw=(1-dlx)*dlz; grid[INSV]+=value*dw; weight[INSV]+=dw;
    // SE
    dw=dlx*dlz; grid[INSE]+=value*dw; weight[INSE]+=dw;
}

int getnearestmarker(double xpos,double zpos) {
    int nearestmarker,e;
    double dist2;
    double mindist2=DBL_MAX;
    for (e=0;e<Ninlimits;e++) {
        dist2=(xpos-mx[ind[e]])*(xpos-mx[ind[e]])+(zpos-mz[ind[e]])*(zpos-mz[ind[e]]);
        if (dist2<mindist2) {mindist2=dist2; nearestmarker=ind[e]; }
    }
    return nearestmarker;
}

void patchgrid(int slocal, int tlocal, double *grid, double *weight, double *mscalar, int isP, int *warning1, int *warning2) {
    int radius, maxradius, Slocal, Tlocal, s, t, st,indomain;
    double newvalue, localweight,xpos,zpos;
    Slocal=Sbase; Tlocal=Tbase; xpos=x[slocal]; zpos=z[tlocal];
//     if (isP) {Slocal--; Tlocal--; xpos+=0.5*dx[slocal]; zpos+=0.5*dz[tlocal];}
    maxradius=3;
    localweight=0; newvalue=0;
    
    for (radius=1;radius<=maxradius;radius++) {
        for (s=slocal-radius; s<=slocal+radius;s++) {
            for (t=tlocal-radius; t<=tlocal+radius;t++) {
                st=IN0(s, t); 
                indomain=(x[s]>=mxmin && x[s]<=mxmax && z[t]>=mzmin && z[t]<=mzmax);
                if (s>=0 && s<Slocal && t>=0 && t<Tlocal && weight[st]>0 && indomain) {
                    localweight++;
                    newvalue+=grid[st];
                }
            }}
                if (localweight>0) break;
    }
    st=IN0(slocal, tlocal);
//     printf("%d %d %d %f\n", slocal, tlocal, radius, localweight);
    if (localweight>0) {grid[st]=newvalue/localweight; *warning1=*warning1+1; }
    else {   grid[st]=mscalar[getnearestmarker(xpos, zpos)]; *warning2=*warning2+1;   }
}
        

void interpolate() {
    int nearest=0;
    int padding=0;
    int patchcount=0;
    int indomain;
    for (e=0;e<Ninlimits;e++)  markertorhogrid(ind[e], mscalar[ind[e]], grid, weight);
    for (s=0;s<Sbase;s++) {
        for (t=0;t<Tbase;t++) {
            st=IN0(s, t);
            indomain=(x[s]>=mxmin && x[s]<=mxmax && z[t]>=mzmin && z[t]<=mzmax);
            if (weight[st]>0 && indomain) grid[st]/=weight[st];

//             if (weight[st]<=0 && indomain)  patchgrid(s, t, grid, weight, mscalar, 0, &nearest, &patchcount);
            if (!indomain) {grid[st]=mxGetNaN(); padding++; }
        }}
//     for (s=0;s<Sbase;s++)  for (t=0;t<Tbase;t++) if (weight[st]<=0) grid[IN0(s, t)]=-3;
    
    for (s=0;s<Sbase;s++) {
        for (t=0;t<Tbase;t++) {
            st=IN0(s, t);
            indomain=(x[s]>=mxmin && x[s]<=mxmax && z[t]>=mzmin && z[t]<=mzmax);
            if (weight[st]<=0 && indomain) patchgrid(s, t, grid, weight, mscalar, 0, &patchcount, &nearest);
//             if (weight[st]>0) grid[st]/=weight[st];
        }
    }
    mexPrintf("Patched points %f Nearest marker interpolation: %f percent. Padding: %f percent\n", (double)patchcount/(double)(Sbase*Tbase)*100, (double)nearest/(double)(Sbase*Tbase)*100, (double)padding/(double)(Sbase*Tbase)*100);
}


int get_s(double xpos) {
    return floor((xpos-xmin)/(xmax-xmin)*(double)(Sbase-1));
}
int get_t(double zpos) {
    return floor((zpos-zmin)/(zmax-zmin)*(double)(Tbase-1));
}

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {
    mscalar=mxGetPr(prhs[0]);
    mx=mxGetPr(prhs[1]);
    mz=mxGetPr(prhs[2]);
    xmin=*mxGetPr(prhs[3]);
    xmax=*mxGetPr(prhs[4]);
    zmin=*mxGetPr(prhs[5]);
    zmax=*mxGetPr(prhs[6]);
    Sbase=*mxGetPr(prhs[7]);
    Tbase=*mxGetPr(prhs[8]);

    
    N=mxGetNumberOfElements(prhs[0]);
    
    plhs[0]=mxCreateDoubleMatrix(Sbase,Tbase, mxREAL);
    plhs[1]=mxCreateDoubleMatrix(Sbase,1, mxREAL);
    plhs[2]=mxCreateDoubleMatrix(Tbase,1, mxREAL);

    
    dx=(xmax-xmin)/(double)(Sbase-1); dz=(zmax-zmin)/(double)(Tbase-1);
    

    x=mxGetPr(plhs[1]); z=mxGetPr(plhs[2]);

    x[0]=xmin; for (s=0;s<Sbase-1;s++) x[s+1]=x[s]+dx;
    z[0]=zmin; for (t=0;t<Tbase-1;t++) z[t+1]=z[t]+dz;
    
    weight=mxCalloc(Sbase*Tbase,sizeof(double)); grid=mxGetPr(plhs[0]);
    
    for (s=0;s<Sbase;s++) {  for (t=0;t<Tbase;t++) { st=IN0(s,t); grid[st]=0; weight[st]=0; } }
    
    ind=mxCalloc(N,sizeof(int));
    Ninlimits=0;
    mxmin=mxGetInf(); mxmax=-mxGetInf(); mzmin=mxGetInf(); mzmax=-mxGetInf();
    for (e=0;e<N;e++) {
        if (mx[e]>xmin && mx[e]<xmax && mz[e]>zmin && mz[e]<zmax) {
        ind[Ninlimits]=e;
        Ninlimits++;
        if (mx[e]<=mxmin) mxmin=mx[e];
        if (mx[e]>=mxmax) mxmax=mx[e];
        if (mz[e]<=mzmin) mzmin=mz[e];
        if (mz[e]>=mzmax) mzmax=mz[e];
        }
    }
    
    
    interpolate();
    mxFree(weight); mxFree(ind);
}
