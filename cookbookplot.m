clear all; %close all;
for count=10:10 %count indicates which file numbers to be read and plotted
    
      % List of outputted properties and how they are opened from a file with number "count". All units are S.I.
%     N=readdata(count,1);         %Number of particles
%     S=readdata(count,2);         %Number of grid nodes in x-direction
%     T=readdata(count,3);         % as above, z-direction
%     multigridlevels=readdata(count,4);         %number of multigrid levels (has no practical use in terms of plotting)
%     bound=readdata(count,5);         %types of boundaries. not important
%     time=readdata(count,6);
%     dtnom=readdata(count,7);         % length of time step used in model run not important for plotting
%     dx=readdata(count,8);         % distance between grid nodes (width of cells) in x-direction: Vector, size S-1
%     dz=readdata(count,9);         %as above, z-direction. size T-1
%     mx=readdata(count,10);         %marker x positions, vector size N (all properties below starting with an are vector size N)
%     mz=readdata(count,11);         %marker z positions, vector size N
%     mcs=readdata(count,12);         %marker belongs to cell with horizontal index specified in this variable (value between 0 and S-1). Can actually be calculated from mx, and dx, so redundant
%     mct=readdata(count,13);         %as above, z-direction
%     mtype=readdata(count,14);         %what type of material are each marker
%     mTemp=readdata(count,15);         % temperature
%     mstressxx=readdata(count,16);         %devitoric stress component simga'xx=-sigma'zz
%     mstressxz=readdata(count,17);         % sigma'xz=sigmaxz
%     mstrainxx=readdata(count,18);         % similar deviatoric strain components (eps'ij=epsij due to incompressibility)
%     mstrainxz=readdata(count,19);
%     mfinitestrain=readdata(count,20);         %accumulated plastic strain (other types of strain are not tracked and saved) 
%     vx=readdata(count,21);         %horisontal velocity grid. Displaced by 0.5 dz relative to primary grid. Matrix, size S*T (includes some dummy points that are outside of the computational domain) 
%     vz=readdata(count,22);         %horisontal velocity grid. Displaced by 0.5 dx relative to primary grid. Matrix, size S*T (includes some dummy points that are outside of the computational domain) 
%     P=readdata(count,23);         % presseure=-0.5(sigma_xx+sigma_zz). displaced 0.5(dx,dz) Matrix, size S*T (includes even more dummy points)
%     Nsurfaces=readdata(count,24);         %Number of semi eulerian surfaces used for tracking interfaces that are not strongly deformed 
%     surfaces=readdata(count,25);         % surfaces. Matrix, size S*Nsurfaces
    
    
%%%%%%%%%%%%%%%%%%%%%%%%
%examples that can be uncommented and applied/modified

% mtype plot
% mx=readdata(count,10); mz=readdata(count,11); mtype=readdata(count,14); scalardotplot(mx,mz,mtype,[0 8]);

% temperature plot
% mx=readdata(count,10); mz=readdata(count,11); mTemp=readdata(count,15); scalardotplot(mx,mz,mTemp,[0 1700]);

% temperature plot with autmatic range
% mx=readdata(count,10); mz=readdata(count,11); mTemp=readdata(count,15); scalardotplot(mx,mz,mTemp,[min(mTemp) max(mTemp)]);

% effective log viscosity plot
mx=readdata(count,10); mz=readdata(count,11);
mstresstot=(readdata(count,16).^2+readdata(count,17).^2).^0.5;
mstraintot=(readdata(count,18).^2+readdata(count,19).^2).^0.5;
mvisc=0.5*mstresstot./mstraintot;
% scalardotplot(mx,mz,log10(0.5*mstresstot./mstraintot),[15 27]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add time in Myr at the top of a plot
% time=readdata(count,6); title([num2str(time/(60*60*24*365*1e6)) ' Myr ']);

%add this to have aspect ratio of 1:
% axis equal;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%more advanced, but good for nicer plots: interpolation from markers to grid

%interpolates a marker scalar (here mtype) value to the grid:
% mtype=readdata(count,14);
% mscalar=mtype;
% S=readdata(count,2); T=readdata(count,3);  
% dx=readdata(count,8); dz=readdata(count,9);
% mx=readdata(count,10); mz=readdata(count,11);
% xmax=sum(dx); zmax=sum(dz);
% xi=linspace(0,xmax,S*2); zi=linspace(0,zmax,T*2); %size of grid need not be 2*S*2*T, but it gives a scaling of the resolution since N is usually proportional to S*T (typically 12-16 markers are present in each cell during initiation)
% F = TriScatteredInterp(mx,mz,mscalar); %calculates interpolation function (based on triangulization)
% [Xi,Zi] = meshgrid(xi,zi); %xi and zi are vectors with the nodes of the grid we have specified. Xi,Zi are matrices with the same, but now redunant values
% grid = F(Xi,Zi); grid=grid';
% imagesc(xi,zi,grid'); caxis([0 1350]); axis(1e3*[0   2000   0 710]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add time in Myr at the top of a plot
time=readdata(count,6); title([num2str(time/(60*60*24*365*1e6)) ' Myr ']); colorbar;

%add this to have aspect ratio of 1:
% axis equal;

pause(0.1);
end