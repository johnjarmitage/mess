#include <stdio.h>
#include "mex.h"
#include "math.h"
//should some integers be unsigned
int N,Sbase,Tbase;
int s,t,st,e,S,T,G,multigridlevels;
int *mcs,*mct,*mtype;
int leftbound,rightbound,upbound,downbound; //0: no slip, 1: free slip 2 free slip/free external slip
double Ttop,Tbottom;
double leftboundvel,rightboundvel,upboundvel,downboundvel;
int *firstelementgrid,*firstelementdx,*firstelementdz,*pow2;
int numberofyieldmarkers,firstmarkerupdate;
int iterationnumber;
double time,dt,dtnom,dtmaxwell;
double *dx,*dz,*x,*z,mincellsize,xmin,xmax,zmin,zmax;
double *mx,*mz,*mTemp,*mstressxx,*mstressxz,*mstrainxx,*mstrainxz;
double *nZn,*nZs,*inZn,*inZs; //ekstra arrays for varying viscosity contrasts
double *weight,*rightP,*rightx,*rightz,*resP,*resx,*resz,*cx0,*cx1,*cx2,*cx3,*cx4,*cx5,*cx6,*cx7,*cx8,*cx9,*cx10,*cz0,*cz1,*cz2,*cz3,*cz4,*cz5,*cz6,*cz7,*cz8,*cz9,*cz10;
double *vx0,*vz0,*P0,*rightx0,*rightz0,*rightP0; //only needed for multigrid
double *P,*vx,*vz;
double *gridviscn,*gridviscs,*gridrho,*gridoldstressxx,*gridoldstressxz,*gridmyn,*gridmys; //some of these grids  do not get allocated independent memory, but is use memory allocated to the cx coeffs which is not used simultaneously
double *gridHs; //global variable since used in both stressstrainupdate and updatetemp
double *mrho,*mvisc,*mmy,*mP,*mfinitestrain;
double gx,gz;
double minvisc,maxvisc,alphaP,alphamom,stressprecision;

#include "momentmummarkerfuncs.c"
#include "solver.c"
#include "tempfuncs.c"
//use other memory fro gridoldstress?


int IN(int s,int t,int G) {
    return t*((Sbase-1)/pow2[G]+1)+s+firstelementgrid[G];
}
int IN0(int s,int t) {
    return t*Sbase+s;
}
int INdx(int s, int G) {
    return s+firstelementdx[G];
}
int INdz(int t, int G) {
    return t+firstelementdz[G];
}
/*
//input: 0: boundary has 0 perpendicular flow. >0: flow out of the model domain. <0: flow into the model domain
void velocityboundarychange(int left,int right,int up,int down,double inwardsvelo) {
    double totalinwardsflux,totaloutwardslength; //absolute values of fluxes are so far considered
    totalinwardsflux=0; totaloutwardslength=0;
    leftboundvel=0; rightboundvel=0; upboundvel=0; downboundvel=0;
    if (left<0) { leftboundvel=inwardsvelo; totalinwardsflux+=inwardsvelo*(zmax-zmin); } else if (left>0) totaloutwardslength+=zmax-zmin;
    if (right<0) { rightboundvel=-inwardsvelo; totalinwardsflux+=inwardsvelo*(zmax-zmin); } else if (right>0) totaloutwardslength+=zmax-zmin;
    if (up<0) { upboundvel=-inwardsvelo; totalinwardsflux+=inwardsvelo*(xmax-xmin); } else if (up>0) totaloutwardslength+=xmax-xmin;
    if (down<0) { downboundvel=inwardsvelo; totalinwardsflux+=inwardsvelo*(xmax-xmin); } else if (down>0) totaloutwardslength+=xmax-xmin;
    //constant outvel: totaloutwardslength*outvel=totalinwardsflux
    if (left>0) leftboundvel=-totalinwardsflux/totaloutwardslength;
    if (right>0) rightboundvel=totalinwardsflux/totaloutwardslength;
    if (up>0) upboundvel=-totalinwardsflux/totaloutwardslength;
    if (down>0) downboundvel=totalinwardsflux/totaloutwardslength;
    mexPrintf("leftboundvel,rightboundvel,upboundvel,downboundvel, %g %g %g %g\n",leftboundvel,rightboundvel,upboundvel,downboundvel);
    mexPrintf("inwards %g net flow %g\n",totalinwardsflux,rightboundvel*(zmax-zmin)-leftboundvel*(zmax-zmin)+downboundvel*(xmax-xmin)-upboundvel*(xmax-xmin));
}
*/
//input: 0: boundary has 0 perpendicular flow. >0: flow out of the model domain. <0: flow into the model domain
void velocityboundarychange(int left,int right,int up,int down,double inwardsvelo) {
    double totalinwardsflux,totaloutwardslength; //absolute values of fluxes are so far considered
    totalinwardsflux=0; totaloutwardslength=0;
    leftboundvel=0; rightboundvel=0; upboundvel=0; downboundvel=0;
    if (left<0) { leftboundvel=inwardsvelo; totalinwardsflux+=inwardsvelo*(zmax-zmin); } else if (left>0) totaloutwardslength+=zmax-zmin;
    if (right<0) { rightboundvel=-inwardsvelo; totalinwardsflux+=inwardsvelo*(zmax-zmin); } else if (right>0) totaloutwardslength+=zmax-zmin;
    if (up<0) { upboundvel=inwardsvelo; totalinwardsflux+=inwardsvelo*(xmax-xmin); } else if (up>0) totaloutwardslength+=xmax-xmin;
    if (down<0) { downboundvel=-inwardsvelo; totalinwardsflux+=inwardsvelo*(xmax-xmin); } else if (down>0) totaloutwardslength+=xmax-xmin;
    //constant outvel: totaloutwardslength*outvel=totalinwardsflux
/*    if (left>0) leftboundvel=-(zmax-zmin)/totaloutwardslength*totalinwardsflux/totaloutwardslength;
    if (right>0) rightboundvel=(zmax-zmin)/totaloutwardslength*totalinwardsflux/totaloutwardslength;
    if (up>0) upboundvel=-(xmax-xmin)/totaloutwardslength*totalinwardsflux/totaloutwardslength;
    if (down>0) downboundvel=(xmax-xmin)/totaloutwardslength*totalinwardsflux/totaloutwardslength;*/
    if (left>0) leftboundvel=-totalinwardsflux/totaloutwardslength;
    if (right>0) rightboundvel=totalinwardsflux/totaloutwardslength;
    if (up>0) upboundvel=-totalinwardsflux/totaloutwardslength;
    if (down>0) downboundvel=totalinwardsflux/totaloutwardslength;
    printf("leftboundvel,rightboundvel,upboundvel,downboundvel, %g %g %g %g\n",leftboundvel,rightboundvel,upboundvel,downboundvel);
    printf("inwards %g net flow %g\n",totalinwardsflux,rightboundvel*(zmax-zmin)-leftboundvel*(zmax-zmin)+downboundvel*(xmax-xmin)-upboundvel*(xmax-xmin));
}

void velocityboundarychange1(int left,int right,int up,int down,double outwardsvelo) {
    double totaloutwardsflux,totalinwardslength; //absolute values of fluxes are so far considered
    totaloutwardsflux=0; totalinwardslength=0;
    leftboundvel=0; rightboundvel=0; upboundvel=0; downboundvel=0;
    if (left>0) { leftboundvel=-outwardsvelo; totaloutwardsflux+=outwardsvelo*(zmax-zmin); } else if (left<0) totalinwardslength+=zmax-zmin;
    if (right>0) { rightboundvel=outwardsvelo; totaloutwardsflux+=outwardsvelo*(zmax-zmin); } else if (right<0) totalinwardslength+=zmax-zmin;
    if (up>0) { upboundvel=-outwardsvelo; totaloutwardsflux+=outwardsvelo*(xmax-xmin); } else if (up<0) totalinwardslength+=xmax-xmin;
    if (down>0) { downboundvel=outwardsvelo; totaloutwardsflux+=outwardsvelo*(xmax-xmin); } else if (down<0) totalinwardslength+=xmax-xmin;

    if (left<0) leftboundvel=totaloutwardsflux/totalinwardslength;
    if (right<0) rightboundvel=-totaloutwardsflux/totalinwardslength;
    if (up<0) upboundvel=totaloutwardsflux/totalinwardslength;
    if (down<0) downboundvel=-totaloutwardsflux/totalinwardslength;
    printf("leftboundvel,rightboundvel,upboundvel,downboundvel, %g %g %g %g\n",leftboundvel,rightboundvel,upboundvel,downboundvel);
    printf("outwards %g net flow %g\n",totaloutwardsflux,rightboundvel*(zmax-zmin)-leftboundvel*(zmax-zmin)+downboundvel*(xmax-xmin)-upboundvel*(xmax-xmin));
}

void initialize() {
    FILE *file;
    file=fopen("0.txt", "rb");
    //Scalar data from beginning of file
    fread(&N,sizeof(int),1,file);
    fread(&Sbase,sizeof(int),1,file);
    fread(&Tbase,sizeof(int),1,file);
    fread(&multigridlevels,sizeof(int),1,file);
    fread(&leftbound,sizeof(int),1,file);
    fread(&rightbound,sizeof(int),1,file);
    fread(&upbound,sizeof(int),1,file);
    fread(&downbound,sizeof(int),1,file);
    fread(&time,sizeof(double),1,file);
    fread(&dtnom,sizeof(double),1,file); dtmaxwell=dtnom;
    //allocation and calculation of basic indexing values
    firstelementgrid=calloc(multigridlevels+2, sizeof(int)); //these show the index of the first element in a grid or dx/dz-vector. Last value [multigridlevels+1] is the total numbers of values
    firstelementdx=calloc(multigridlevels+2, sizeof(int));
    firstelementdz=calloc(multigridlevels+2, sizeof(int));
    pow2=calloc(multigridlevels+1, sizeof(int));
    S=Sbase;
    T=Tbase;
    G=0;
    firstelementgrid[0]=0;
    firstelementdx[0]=0;
    firstelementdz[0]=0;
    firstelementgrid[1]=Sbase*Tbase;
    firstelementdx[1]=Sbase-1;
    firstelementdz[1]=Tbase-1;
    for (G=2;G<=multigridlevels+1;G++){
        if ((S-1)%2!=0 || (T-1)%2!=0 || S==1 || T==1) {
            printf("Error: multigridlevel too high or bad array size\n"); }
        S=(S-1)/2+1;
        T=(T-1)/2+1;
        firstelementgrid[G]=firstelementgrid[G-1]+S*T;
        firstelementdx[G]=firstelementdx[G-1]+S-1;
        firstelementdz[G]=firstelementdz[G-1]+T-1;
    }
    for (G=0;G<=multigridlevels+1;G++){
        printf("%d %d %d\n",firstelementgrid[G],firstelementdx[G],firstelementdz[G]); }
    pow2[0]=1; //Is used in the function IN() since pow() takes many ressources
    for (G=1;G<=multigridlevels;G++){
        pow2[G]=pow2[G-1]*2; }
    //allocation of grid memory
    dx=calloc(firstelementdx[multigridlevels+1],sizeof(double));
    dz=calloc(firstelementdz[multigridlevels+1],sizeof(double));
    P=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    vx=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    vz=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    nZs=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    nZn=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    rightP=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    rightx=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    rightz=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    resP=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    resx=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    resz=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cx0=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cx1=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cx2=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cx3=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cx4=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cx5=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cx6=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cx7=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cx8=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cx9=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cx10=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cz0=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cz1=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cz2=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cz3=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cz4=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cz5=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cz6=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cz7=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cz8=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cz9=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    cz10=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    P0=calloc(firstelementgrid[1],sizeof(double));
    vx0=calloc(firstelementgrid[1],sizeof(double));
    vz0=calloc(firstelementgrid[1],sizeof(double));
    rightx0=calloc(firstelementgrid[1],sizeof(double));
    rightz0=calloc(firstelementgrid[1],sizeof(double));
    rightP0=calloc(firstelementgrid[1],sizeof(double));
    weight=calloc(firstelementgrid[multigridlevels+1],sizeof(double));
    P0=calloc(firstelementgrid[1],sizeof(double));
    vx0=calloc(firstelementgrid[1],sizeof(double));
    vz0=calloc(firstelementgrid[1],sizeof(double));
    rightx0=calloc(firstelementgrid[1],sizeof(double));
    rightz0=calloc(firstelementgrid[1],sizeof(double));
    rightP0=calloc(firstelementgrid[1],sizeof(double));
    gridrho=calloc(firstelementgrid[1],sizeof(double));
    gridoldstressxx=calloc(firstelementgrid[1],sizeof(double));
    gridoldstressxz=calloc(firstelementgrid[1],sizeof(double));
    gridviscn=calloc(firstelementgrid[1],sizeof(double));
    gridviscs=calloc(firstelementgrid[1],sizeof(double));
    gridmyn=calloc(firstelementgrid[1],sizeof(double));
    gridmys=calloc(firstelementgrid[1],sizeof(double));
    inZn=calloc(firstelementgrid[1],sizeof(double));
    inZs=calloc(firstelementgrid[1],sizeof(double));
    //allocation and input of grid and marker data
    for (s=0;s<Sbase-1;s++) fscanf(file,"%lg",&dx[s]);
    for (t=0;t<Tbase-1;t++) fscanf(file,"%lg",&dz[t]);
    mx=calloc(N,sizeof(double)); for (e=0;e<N;e++) fscanf(file,"%lg",&mx[e]);
    mz=calloc(N,sizeof(double)); for (e=0;e<N;e++) fscanf(file,"%lg",&mz[e]);
    mcs=calloc(N,sizeof(int)); for (e=0;e<N;e++) fscanf(file,"%d",&mcs[e]);
    mct=calloc(N,sizeof(int)); for (e=0;e<N;e++) fscanf(file,"%d",&mct[e]);
    mtype=calloc(N,sizeof(int)); for (e=0;e<N;e++) fscanf(file,"%d",&mtype[e]);
    mTemp=calloc(N,sizeof(double)); for (e=0;e<N;e++) fscanf(file,"%lg",&mTemp[e]);
    mstressxx=calloc(N,sizeof(double)); for (e=0;e<N;e++) fscanf(file,"%lg",&mstressxx[e]);
    mstressxz=calloc(N,sizeof(double)); for (e=0;e<N;e++) fscanf(file,"%lg",&mstressxz[e]);
    mstrainxx=calloc(N,sizeof(double)); for (e=0;e<N;e++) fscanf(file,"%lg",&mstrainxx[e]);
    mstrainxz=calloc(N,sizeof(double)); for (e=0;e<N;e++) fscanf(file,"%lg",&mstrainxz[e]);
    mfinitestrain=calloc(N,sizeof(double)); for (e=0;e<N;e++) fscanf(file,"%lg",&mfinitestrain[e]);
    //allocation and input of grid and marker data
    fread(dx,sizeof(double),Sbase-1,file);
    fread(dz,sizeof(double),Tbase-1,file);
    mx=calloc(N,sizeof(double)); fread(mx,sizeof(double),N,file);
    mz=calloc(N,sizeof(double)); fread(mz,sizeof(double),N,file);
    mcs=calloc(N,sizeof(int)); fread(mcs,sizeof(int),N,file);
    mct=calloc(N,sizeof(int)); fread(mct,sizeof(int),N,file);
    mtype=calloc(N,sizeof(int)); fread(mtype,sizeof(int),N,file);
    mTemp=calloc(N,sizeof(double)); fread(mTemp,sizeof(double),N,file);
    mstressxx=calloc(N,sizeof(double)); fread(mstressxx,sizeof(double),N,file);
    mstressxz=calloc(N,sizeof(double)); fread(mstressxz,sizeof(double),N,file);
    mstrainxx=calloc(N,sizeof(double)); fread(mstrainxx,sizeof(double),N,file);
    mstrainxz=calloc(N,sizeof(double)); fread(mstrainxz,sizeof(double),N,file);
    mfinitestrain=calloc(N,sizeof(double)); fread(mfinitestrain,sizeof(double),N,file);
    //allocation of additional marker memory
    mvisc=calloc(N,sizeof(double));
    mmy=calloc(N,sizeof(double));
    mrho=calloc(N,sizeof(double));
    mP=calloc(N,sizeof(double));
    //input of grid data
    fread(vx,sizeof(double),Sbase*Tbase,file);
    fread(vz,sizeof(double),Sbase*Tbase,file);
    fread(P,sizeof(double),Sbase*Tbase,file);
    fclose(file);
    //rho nodes position on the grid
    x=calloc(Sbase,sizeof(double)); x[0]=0; for (s=0;s<Sbase-1;s++) x[s+1]=x[s]+dx[s];
    z=calloc(Tbase,sizeof(double)); z[0]=0; for (t=0;t<Tbase-1;t++) z[t+1]=z[t]+dz[t];
    xmin=x[0]; xmax=x[Sbase-1]; zmin=z[0]; zmax=z[Tbase-1];
    //dx and dz on coarse scale are calculated
    for (G=1;G<=multigridlevels;G++){
        S=(Sbase-1)/pow2[G]+1;
        T=(Tbase-1)/pow2[G]+1;
        for (s=0;s<S-1;s++) {
            dx[INdx(s,G)]=dx[INdx(2*s,G-1)]+dx[INdx(2*s+1,G-1)]; }
        for (t=0;t<T-1;t++) {
            dz[INdz(t,G)]=dz[INdz(2*t,G-1)]+dz[INdz(2*t+1,G-1)]; }
    }
    mincellsize=x[Sbase-1]+z[Tbase-1]; //this value will always be reduced in the following loops
    for (s=0;s<Sbase-1;s++) {if (mincellsize>dx[s]) {mincellsize=dx[s]; }}
    for (t=0;t<Tbase-1;t++) {if (mincellsize>dz[t]) {mincellsize=dz[t]; }}
    //assignment of constants
    leftboundvel=0; rightboundvel=0; upboundvel=0; downboundvel=0;
    alphaP=0.1; alphamom=0.5;
    gx=0; gz=9.82;
}



double getmarkerdevstressinv2(int e,double visc,double my) { //second stress invariant squared
    double stressxx,stressxz;
    stressxx=2*visc*mstrainxx[e]*dt*my/(dt*my+visc)+mstressxx[e]*(visc/(dt*my+visc));
    stressxz=2*visc*mstrainxz[e]*dt*my/(dt*my+visc)+mstressxz[e]*visc/(dt*my+visc);
    return secdevinv2(stressxx,stressxz);
}

double nonlinvisc(int e,double n,double Fn,double Ad,double E,double V,double my) { //Fn is F^n
    double P,T;
    double R;
    double visc,sigmaII,lowsigma,highsigma,sigmaold;
    int i;
    P=mP[e]; T=mTemp[e]+273;
    R=8.314472;
    sigmaII=sqrt(secdevinv2(mstressxx[e],mstressxz[e]));
    lowsigma=sigmaII; highsigma=sigmaII;
    
    visc=pow(2/sigmaII,(n-1)/1)*Fn/Ad*exp((E+P*V)/(R*T));
    if (isinf(visc)) visc=DBL_MAX; //if the stress is very low, the viscosity may become infinite
    sigmaII=sqrt(getmarkerdevstressinv2(e,visc,my));
    if (sigmaII>highsigma) highsigma=sigmaII;
    if (sigmaII<lowsigma) lowsigma=sigmaII;
    visc=pow(2/sigmaII,(n-1)/1)*Fn/Ad*exp((E+P*V)/(R*T));
    if (isinf(visc)) visc=DBL_MAX; //if the stress is very low, the viscosity may become infinite
    sigmaII=sqrt(getmarkerdevstressinv2(e,visc,my));
    if (sigmaII>highsigma) highsigma=sigmaII;
    if (sigmaII<lowsigma) lowsigma=sigmaII;
    
    i=0;
    while (highsigma-lowsigma>1*stressprecision && i<100) {
        i++;
        sigmaII=0.5*(highsigma+lowsigma); sigmaold=sigmaII;
        visc=pow(2/sigmaII,(n-1)/1)*Fn/Ad*exp((E+P*V)/(R*T));
        sigmaII=sqrt(getmarkerdevstressinv2(e,visc,my));
        if (sigmaII>sigmaold) lowsigma=sigmaold; else highsigma=sigmaold;
    }
    return visc;
}

double frictionsoftening(double phi_peak,double phi_stable,double totalstrain) {
    double emax,emin,output;
    emax=0.5; emin=1e-3;
    if (totalstrain>emin) {
        if (totalstrain<emax) output=phi_peak+(totalstrain-emin)*(phi_stable-phi_peak)/(emax-emin);
        else output=phi_stable;
    }
    else
        output=phi_peak;
    return output;
}


int calculatemarkerparams(int yield) { //if yield is 1, effective yielding viscosities will be calculated
    double visc,my,rho,rho0,viscmin,viscmax;
    double Ad,E,V,sigmayield,n; //let R be global. implement with sigmayield later
    double factor,highvisc,lowvisc,sigmaII2;
    int yielditeration;
    int numberoffirsttimeyieldmarkers=0;
    viscmin=1e-15; viscmax=1e80; //change!
    for (e=0;e<N;e++) {
        sigmayield=DBL_MAX;
        switch(mtype[e]) {
 case 0:
     visc=1e+20;
     my=1e+10;
     rho=1;
     break;
            case 1:
                //visc=1e+24;
                my=2.5e+10;
                visc=nonlinvisc(e,2.3,pow(2,1-2*2.3),5.0119e-018,154000,0,my);
                //visc=1e+27;
                rho0=2800;
                //rho=rho0;
                rho=rho0-3.28e-5*mTemp[e];
                sigmayield=21.6e+6+sin(0.0175*frictionsoftening(36,30,mfinitestrain[e]))*mP[e];
                if (sigmayield<0) sigmayield=21.6e+6;
                break;
            case 2:
                //visc=1e+24;
                my=2.5e+10;
                visc=nonlinvisc(e,3.2,pow(2,1-2*3.2),1.9953e-023,238000,0,my);
                //visc=1e+27;
                rho0=3000;
                //rho=rho0;
                rho=rho0-3.28e-5*mTemp[e];
                sigmayield=21.6e+6+sin(0.0175*frictionsoftening(36,30,mfinitestrain[e]))*mP[e];
                if (sigmayield<0) sigmayield=21.6e+6;
                break;
            case 3:
                //visc=1e+30;
                my=6.7e+10;
                visc=nonlinvisc(e,3.5,pow(2,1-2*3.5),2.5119e-017,532000,8e-8,my);
                //visc=1e+17;
                rho0=3300;
                //rho=rho0;
                //sigmayield=21.6e+6+sin(0.0175*frictionsoftening(36,30,mfinitestrain[e]))*mP[e];
                if (sigmayield<0) sigmayield=21.6e+6;
                rho=rho0-3.28e-5*mTemp[e];
                break;
            case 4:
                //visc=1e+30;
                my=6.7e+10;
                visc=nonlinvisc(e,3.5,pow(2,1-2*3.5),2.5119e-017,532000,8e-8,my);
                //visc=1e+17;
                rho0=3300;
                //rho=rho0;
                //sigmayield=21.6e+6+sin(0.0175*frictionsoftening(36,30,mfinitestrain[e]))*mP[e];
                if (sigmayield<0) sigmayield=21.6e+6;
                rho=rho0-3.28e-5*mTemp[e];
                break;
                
                default:
                    visc=1e+20;
                    my=1e+10;
                    rho=2800;
        }

        sigmaII2=getmarkerdevstressinv2(e,visc,my);
        if (sigmaII2<=sigmayield*sigmayield) {
            mfinitestrain[e]=-fabs(mfinitestrain[e]); } //no yielding. finite strain becomes negative to indicate this
        else {
            numberofyieldmarkers++;
            if  (mfinitestrain[e]<=0) {
                numberoffirsttimeyieldmarkers++;
                if (yield)          {
                    mfinitestrain[e]=fabs(mfinitestrain[e]); } //the number of first time yielding markers is counted. if yield is activated in the function call, mfinitestrain turns positive
            } }
        if (sigmaII2>sigmayield*sigmayield && yield) { // yielding if marker is above yield and yield is activated from the function call
            factor=sigmayield/sqrt(secdevinv2(mstressxx[e],mstressxz[e]));
            visc=sigmayield/(2*sqrt(secdevinv2(mstrainxx[e],mstrainxz[e])));
            mstressxx[e]*=factor; mstressxz[e]*=factor;
            
        }
        if (visc<viscmin) visc=viscmin;
        if (visc>viscmax) visc=viscmax;
        mvisc[e]=visc; mmy[e]=my; mrho[e]=rho;
    }
    return numberoffirsttimeyieldmarkers;
}

void firsttimesteps() { //number of short timesteps to make elastic stress buildups
    double olddtnom;
    int a;
    olddtnom=dtnom; //backup of nominal timestep
    for (dtnom=0.00001*olddtnom;dtnom<olddtnom;dtnom*=10){
        for (a=0;a<1;a++) {
            timestep(); }
    }
    dtnom=olddtnom;
}

void printgrid(double *grid,int G) {
    int S,T;
    S=(Sbase-1)/pow2[G]+1;
    T=(Tbase-1)/pow2[G]+1;
    printf("\n");
    for (t=0;t<T;t++)	{
        for (s=0;s<S;s++){
            printf("%g ",grid[IN(s,t,G)]);
        }
        printf("\n");
    }
}

void timestep() {
    int accetable_vel;
    int timestepsbetweenprint;
    printf("ITERATION: %d\n",iterationnumber); 
    timestepsbetweenprint=5;
    if (iterationnumber%timestepsbetweenprint==0) printstate(iterationnumber/timestepsbetweenprint);
    
    
    accetable_vel=0;
    if (time<50000000/3.17e-8) velocityboundarychange1(0,1,0,-1,0.01*3.17e-8); else velocityboundarychange1(0,1,0,-1,0);

    while (accetable_vel==0) {
        markermomentumparamstogrid();
        printf("time:%g seconds. dt: %g seconds. dtnom %g seconds dtmaxwell %g seconds\n",time,dt,dtnom,dtmaxwell); 
        solve(stressprecision);
        printf("max vel %g, dt %g, mincellsize %g\n",maxvelocity(),dt,mincellsize);
        if (dt*maxvelocity()<1*mincellsize) accetable_vel=1;
    }
    stressstrainupdate();
    updatetemp();
    stressrotation();
    movemarkers1();
    printf("number of yield markers %d\n\n",numberofyieldmarkers); 
    time+=dt; iterationnumber++;
    
}


void main()
{
    int a,gravityon;
    double lengthx,lengthz;
    double dtnomold;
    initialize();
    printf("after initialize\n");
    dtnomold=dtnom; //this may be deleted again
    stressprecision=1e5;
    
    Ttop=0; Tbottom=1400;
    
    iterationnumber=0;
    gz=9.82;
    //gx=9.82;
    //dtnom=1e+3*60*60*24*365;
    firsttimesteps();
    for (a=0;time<6e+7*60*60*24*365;a++) {
        if (iterationnumber>100000) break;
        timestep();
    }

}