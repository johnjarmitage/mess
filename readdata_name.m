function out=readdata_name(dirname,filenumber,varnumber);
filename=[num2str(filenumber) '.txt'];
fid=fopen([dirname filename],'rb');
N=fread(fid,1,'int32');
S=fread(fid,1,'int32');
T=fread(fid,1,'int32');
[begin type]=firstbytes(N,S,T);

if type(varnumber)==0
    Nel=begin(varnumber+1)-begin(varnumber);
    fseek(fid,begin(varnumber),-1);
    Nbytes=Nel/4;
    out=fread(fid,Nbytes,'int32');
end
if type(varnumber)==1
    Nel=begin(varnumber+1)-begin(varnumber);
    fseek(fid,begin(varnumber),-1);
    Nbytes=Nel/8;
    out=fread(fid,Nbytes,'real*8');
end
if type(varnumber)==2
    Nel=begin(varnumber+1)-begin(varnumber);
    fseek(fid,begin(varnumber),-1);
    Nbytes=Nel/8;
    out=fread(fid,Nbytes,'real*8'); out=reshape(out,S,T);
end
if type(varnumber)==3
    fseek(fid,begin(24),-1);
    Nsurfaces=fread(fid,1,'int32');
    out=fread(fid,Nsurfaces*S,'real*8'); out=reshape(out,S,Nsurfaces);
end
fclose(fid);
